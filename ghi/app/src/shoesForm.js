import React, { useEffect, useState } from 'react';

function ShoeForm() {
  const [color, setColor] = useState('');
  const [modelName, setModelName] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [pictureUrl, setPictureUrl] = useState('')
  const [bins, setBins] = useState([]);
  const [bin, setBin] = useState('');

  const fetchData = async () => {
    const binsUrl = "http://localhost:8100/api/bins/";
    const response = await fetch(binsUrl);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
      console.log(data);
    }
  }
  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.model_name = modelName;
    data.manufacturer = manufacturer;
    data.picture_url = pictureUrl;
    data.bins = bin;

    console.log(data);
    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      setColor('');
      setModelName('');
      setManufacturer('');
      setPictureUrl('');
      setBin('');
    }
  }

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handleModelNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  }
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input value={modelName} onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture Url</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <select value={bin} onChange={handleBinChange} required name="bins" id="bins" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;