from django.db import models
from django.urls import reverse

# Create your models here.

class BinsVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)


    bins = models.ForeignKey(
        BinsVO,
        null=True,
        related_name="bins",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.model_name
    
    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
