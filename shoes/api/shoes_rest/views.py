from django.shortcuts import render
from .models import Shoes, BinsVO
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
# Create your views here.

class BinsVODetailEncoder(ModelEncoder):
    model = BinsVO
    properties = ["closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = ["model_name", "color", "picture_url", "id"]

    def get_extra_data(self, o):
        return {"closet_name": o.bins.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["color", "model_name", "manufacturer", "picture_url", "bins"]
    encoders= {
        "bins": BinsVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bins_href = content["bins"]
            bins = BinsVO.objects.get(import_href=bins_href)
            content["bins"] = bins
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bins id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
       
@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    

    

       