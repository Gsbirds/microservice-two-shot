from django.shortcuts import render
from django.http import JsonResponse
import json
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.shortcuts import render, redirect, get_object_or_404
from .models import Hat, LocationVO
from common.json import ModelEncoder
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model=LocationVO
    properties=[
        "import_href",
        "closet_name"
    ]
class HatsListEncoder(ModelEncoder):
    model=Hat
    properties=[
        "style_name",
        "fabric",
        "picture",
        "id",
        "color"
       
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model=Hat
    properties=[
        "style_name",
        "fabric",
        "color",
        "picture",
        "location",
        
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )

        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
    
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_hats_detail(request, pk):
    if request.method == "GET":
        posts = Hat.objects.get(id=pk)
        return JsonResponse(
            posts, encoder=HatDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        posts = Hat.objects.get(id=id)
        
        return JsonResponse(
            posts,
            encoder=HatDetailEncoder,
            safe=False,
        )
   