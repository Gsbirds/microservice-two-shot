import React, { useState, useEffect } from "react";

function HatsList() {
  const [files, setFiles] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setFiles(data.hats);
      console.log(files);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDeleteButton = async (e) => {
    const { id } = e.target;
    alert("Hat deleted!");
    const locationUrl = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setFiles(files.filter(hat=>(hat.id!== parseInt(id))))
    }
  };
  const styleObj = {
    width: 100,
    borderRadius: 10,
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Location</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {files.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.fabric}</td>
              <td>{hat.location}</td>
              <td>
                <img style={styleObj} src={hat.picture} alt="hat picture" />
              </td>
              <td>
                <button
                  className="btn btn-danger"
                  id={hat.id}
                  onClick={handleDeleteButton}
                >
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
