from django.contrib import admin
from .models import Hat, LocationVO

# Register your models here.
class HatAdmin(admin.ModelAdmin):
    list_display=(
        "fabric",
        "style_name",
        "color",
        "location",
        "id"
    )
admin.site.register(Hat, HatAdmin)

class LocationVOAdmin(admin.ModelAdmin):
    list_display=(
        "closet_name",
        "id",
    )
admin.site.register(LocationVO, LocationVOAdmin)