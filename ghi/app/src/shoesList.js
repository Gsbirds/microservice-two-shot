import React, { useState, useEffect } from "react";

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {

        const shoes_url = "http://localhost:8080/api/shoes/";

        const response = await fetch(shoes_url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);
    const handleDeleteButton = async (event) => {
        const { id } = event.target;
        const resp = await fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: "delete"

        })
        if (resp.ok) {
            const data = await resp.json();
            //fetchData();
            setShoes(shoes.filter(shoe => (shoe.id !== parseInt(id))));

        };

    }
    const styleObj = {
        width: 100,
        textAlign: "center",
        borderRadius: 10,
    }
    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Closet</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td>{shoe.id}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.closet_name}</td>
                                <td><img style={styleObj} src={shoe.picture_url} alt="picture" /></td>
                                <td><button className="btn btn-danger" id={shoe.id} onClick={handleDeleteButton}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ShoesList;