from django.db import models
# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

    # def get_api_url(self):
    #     return reverse("api_location", kwargs={"pk": self.pk})
    
    # def __str__(self):
    #     return str(self.closet_name)

class Hat(models.Model):
    fabric= models.CharField(max_length=200)
    style_name= models.CharField(max_length=200)
    color=models.CharField(max_length=100)
    picture=models.URLField(null=True)
    location=models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete= models.CASCADE
    )
    
    def get_api_url(self):
        return reverse("show_works", kwargs={"id": self.id})
    def __str__(self):
        return str(self.style_name)





# Create your models here.
