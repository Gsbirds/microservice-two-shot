from django.urls import path, include
from .views import api_list_hats, api_hats_detail

urlpatterns = [
    path("hats/", api_list_hats, name="list_hats"),
    path("hats/<int:pk>/", api_hats_detail, name="detail_hats"),

]
