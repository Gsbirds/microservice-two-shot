import React, { useEffect, useState } from "react";
function HatForm() {
  const [style, setStyle] = useState("");
  const [fabric, setFabric] = useState("");
  const [picture, setPicture] = useState("");
  const [color, setColor] = useState("");
  const [location, setLocation] = useState("");
  const [locations, setLocations] = useState([]);

  const handleStyleChange = (event) => {
    const value = event.target.value;
    setStyle(value);
  };
  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };
  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  };
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    // create an empty JSON object
    const data = {};

    data.style_name = style;
    data.fabric = fabric;
    data.picture = picture;
    data.location = location;
    data.color = color;

    console.log(data);

    const hatsUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);
      setStyle("");
      setFabric("");
      setColor("");
      setPicture("");
      setLocation("");
    }
  };
  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
      console.log(data);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Make a Hat</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFabricChange}
                value={fabric}
                placeholder="Presenter name"
                required type="text"
                id="presenter_name"
                className="form-control"
              />
              <label htmlFor="presenter_name">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStyleChange}
                value={style}
                placeholder="Style"
                required type="text" 
                id="Style"
                className="form-control"
              />
              <label htmlFor="presenter_name">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={color}
                placeholder="Color"
                required type="text" 
                id="Color"
                className="form-control"
              />
              <label htmlFor="presenter_name">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="url"
                onChange={handlePictureChange}
                value={picture}
                placeholder="Picture"
                id="Picture"
                className="form-control"
              />
              <label htmlFor="presenter_name">Picture</label>
            </div>

            <div className="mb-3">
              <select
                className="form-select"
                onChange={handleLocationChange}
                name="location"
                id="location"
                // className="form-select "
                required type="text" 
                value={location}
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.href} value={location.href}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
