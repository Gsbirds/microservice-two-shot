from django.contrib import admin
from .models import Shoes, BinsVO

# Register your models here.

@admin.register(Shoes)
class ShoesAdmin(admin.ModelAdmin):
    list_display=["model_name", "id"]

@admin.register(BinsVO)
class BinsVOAdmin(admin.ModelAdmin):
    pass